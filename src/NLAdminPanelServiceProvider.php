<?php

namespace hpsadev\NLAdminPanel;

use Illuminate\Support\ServiceProvider;

class NLAdminPanelServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {

        if (!$this->app->routesAreCached()) {
            require __DIR__.'/routes.php';
        }

        $this->loadViewsFrom(__DIR__.'/Views', 'nladminpanel');

        $this->publishes([
            __DIR__.'/Views'                        => base_path("resources/views/vendor/nladminpanel"),
            __DIR__.'/Assets/admin_finance.js'      => public_path().'/js/admin_finance.js',
            __DIR__.'/Assets/admin_gov_docs.js'     => public_path().'/js/admin_gov_docs.js',
            __DIR__.'/Assets/admin_approvals.js'    => public_path().'/js/admin_approvals.js',
            __DIR__.'/Assets/admin_login.js'        => public_path().'/js/admin_login.js'
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
