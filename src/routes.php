<?php

Route::get('/admin', ['middleware' => 'publicRedirect', function () { return view('nladminpanel::login'); }]);

/*************************
      ADMIN GET
/*************************/
Route::get('/admin/dashboard', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@adminDashboard');
Route::get('/admin/auth/logout', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@logoutAdmin');
Route::get('/admin/profile/{profileId}/application/important-information', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@importnantInfoView');
Route::get('/admin/profile/{profileId}/application/personal-data', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@personalDataView');
Route::get('/admin/profile/{profileId}/application/medical-details', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@medicalDetailsView');
Route::get('/admin/profile/{profileId}/application/family-members', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@familyMembersView');
Route::get('/admin/profile/{profileId}/application/business', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@businessView');
Route::get('/admin/profile/{profileId}/application/declarations', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@declarationsView');
Route::get('/admin/profile/{profileId}/application/approvals', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@approvals');
Route::get('/admin/profile/{profileId}/application/finance', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@financeView');
Route::get('/admin/profile/{profileId}/application/government-docs', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@governmentDocs');

/*************************
      ADMIN POST
/*************************/
Route::post('/admin', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@adminLogin');
Route::post('/admin/send-pin', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@sendPin');
Route::post('/admin/profile/{profileId}/application/important-information', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@importnantInfoPost');
Route::post('/admin/profile/{profileId}/application/personal-data', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@personalDataPost');
Route::post('/admin/profile/{profileId}/application/medical-details', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@medicalDetailsPost');
Route::post('/admin/profile/{profileId}/application/family-members', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@familyMembersPost');
Route::post('/admin/profile/{profileId}/application/business', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@businessPost');
Route::post('/admin/profile/{profileId}/application/declarations', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@declarationsPost');

Route::post('/admin/profile/{profileId}/application/finance', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@updateAccBalance');

Route::post('/admin/profile/{profileId}/application/update-state', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@updateState');
Route::post('/admin/profile/{profileId}/application/send-email', 'hpsadev\NLAdminPanel\Controllers\NLAdminPanelController@sendEmail');
