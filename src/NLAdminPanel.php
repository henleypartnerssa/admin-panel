<?php

namespace hpsadev\NLAdminPanel;

use Hash;
use Crypt;
use App\Group;
use App\User;
use App\Profile;
use hpsadev\Mailer\Mailer;

/**
 * This Package Class contains general static functions used
 * throughout the Admin Panel funcitonality.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.2
 */
class NLAdminPanel
{

    /**
     * Validates if Admin User exists and if not create
     * admin user from system settings.
     *
     * @since  0.0.2 Function Init
     * @return bool User creation idincator
     */
    public static function validateAdminUser() {
        $group  = Group::where('roles', 'admin')->get()->first();
        $count  = User::where('group_id', $group->group_id)->count();
        if ($count === 0) {
            $user   = new User();
            $user->group_id         = $group->group_id;
            $user->name             = "Newlands";
            $user->surname          = "Global";
            $user->username         = trim(env('ADMIN_USER_NAME'));
            $user->password         = trim(env('ADMIN_USER_PASSWORD'));
            $user->token            = Hash::make("admin");
            $user->remember_token   = Hash::make("admin");
            $user->pin              = trim(env('ADMIN_USER_FIRST_PIN'));
            $user->cell_no          = trim(env('ADMIN_USER_CELL'));
            $user->doc_pass         = Crypt::encrypt(trim(env('ADMIN_USER_PASSWORD')));
            $saved = $user->save();
            if ($saved) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Update User Application State
     * @since  0.0.2 Function Init
     * @param  integer $profileID ProfileID
     * @param  POST $request   POST data
     */
    public static function updateState($profileID, $request) {
        $app = Profile::find($profileID)->application;
        $answer = true;

        if ($request['rollback'] === 'true') {
            $answer = false;
        }
        if ($request['override'] === '1') {
            $app->update([
                'step' => NLAdminPanel::lkpApprovalNo($request['approval']),
                NLAdminPanel::lkpAppStateColumn($request['approval']) => $answer
            ]);
        } else {
            $app->update([
                NLAdminPanel::lkpAppStateColumn($request['approval']) => $answer
            ]);
        }
    }

    // NOTE: not sure if this function still in use ?
    public static function mailer($profileID, $request) {
        $profile    = Profile::find($profileID);
        $user       = $profile->user;
        if ($request['approval'] === 'dd') {
            Mailer::ddCheckUpdate([
                'user' => $user,
                'data' => [
                    'title'     => $profile->title,
                    'name'      => $profile->first_name,
                    'surname'   => $profile->last_name
                ]
            ]);
        }
    }

    /**
     * Validates of specified step no is great/lessor than
     * current application state.
     *
     * @since  0.0.2 Function Init
     * @param  integer $profileID    ProfileID
     * @param  string $approvalType Approval Type
     * @return integer               state indicator
     */
    public static function validateState($profileID, $approvalType) {
        $profile    = Profile::find($profileID);
        $step       = $profile->application->step;
        $apprStep   = NLAdminPanel::lkpApprovalNo($approvalType);
        if ($step > $apprStep) {
            return '-1'; // warning greater than.
        } else if ($step < $apprStep) {
            return '-2'; // warning less than
        } else {
            return '1'; // okay
        }
    }

    /**
     * Lookup applicaitons table column by specified approval.
     * @since  0.0.2 Function Init
     * @param  string $approvalType Approval Type
     * @return string               Column name
     */
    public static function lkpAppStateColumn($approvalType) {
        $return['dd']           = 'dd_check';
        $return['retainer']     = 'retainer_paid';
        $return['dr']           = 'data_review';
        $return['balance']      = 'balance_paid';
        $return['gov_docs']     = 'docs_received';
        $return['review']       = 'application_review';
        $return['submitted']    = 'submitted_to_gov';
        $return['ciu']          = 'ciu_approval';
        $return['investment']   = 'investment_completed';
        $return['oath']         = 'oath';
        $return['passport']     = 'passport_issued';
        $return['dominica']     = 'sent_to_dominica';
        return $return[$approvalType];
    }

    /**
     * Lookup step no by approval approval Type
     * @since  0.0.2 Function Init
     * @param  string $approvalType Approval Type
     * @return integer               step no
     */
    public static function lkpApprovalNo($approvalType) {
        switch ($approvalType) {
            case 'dd':
                return 3;
                break;
            case 'retainer':
                return 4;
                break;
            case 'dr':
                return 7;
                break;
            case 'balance':
                return 10;
                break;
            case 'gov_docs':
                return 11;
                break;
            case 'review':
                return 12;
                break;
            case 'ciu':
            case 'dominica':
            case 'submitted':
                return 13;
                break;
            case 'investment':
                return 14;
                break;
            case 'oath':
                return 15;
                break;
            case 'passport':
                return 16;
                break;
            default:
                return 'N/A';
                break;
        }
    }

}
