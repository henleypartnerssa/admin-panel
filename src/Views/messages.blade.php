@extends('nladminpanel::app_left')

@section('title', 'Applicants Messages')

@section('left')
    @include('nladminpanel::_left', $data = ['link' => 'message', 'appId' => $appId])
@stop

@section('content')
    <div class="ui container">
        <div class="ui clearing segment">
            <h3 class="ui left floated header">
              Antigua and Barbuda - {{$profile->first_name}} {{$profile->last_name}} {{$profile->belongs_to}}
          </h3>
            <h3 class="ui right floated header">
              Primary Applicant - Dependants
          </h3>
        </div>

        <div class="ui bottom attached segment">

            <div class="ui middle aligned selection list">

                @foreach($listMessages as $message)
                <a class="item" style="text-align: left" id="{{$message['id']}}" href="/admin/messages/{{$message['id']}}">
                    <div class="right floated content">
                        <small style="float:right"><b>{{$message['timestamp']}}</b></small>
                    </div>
                    @if($message['read'] === false)
                        <i class="ui avatar image mail icon"></i>
                    @else
                        <i class="ui avatar image mail outline icon"></i>
                    @endif
                    <div class="content">
                        <div class="header">From: {{$message['from']}}</div>
                        <div class="description">Subject: {{$message['subject']}}</div>
                    </div>
                </a>
                @endforeach
            </div>

        </div>
@stop

@section('script')
    <!--script src="{{ URL::asset('js/admin/approvals.js') }}"></script-->
@stop
