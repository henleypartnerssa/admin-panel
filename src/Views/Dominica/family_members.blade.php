@extends('nladminpanel::app_left')

@section('title', 'Family Members')

@section('left')
    @include('nladminpanel::_left', $data = ['link' => 'data'])
@stop

@section('content')

<h3 class="ui center aligned block header">{{$profile->first_name}} {{$profile->last_name}} - Family Members</h3>

<div class="ui stacked segment">

    @if($user->profiles()->count() > 4)
        @include('nladminpanel::_dep_guide_split', $d = ['page' => 'family-members'])
    @else
        @include('nladminpanel::_dep_guide', $d = ['page' => 'family-members'])
    @endif

    <div class="ui top attached tabular menu">
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/important-information">
            Important Information
            </a>
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/personal-data">
            Perosnal Details
            </a>
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/medical-details">
            Medical Details
            </a>
            <a class="active item" href="/admin/profile/{{$profile->profile_id}}/application/family-members">
            Family Details
            </a>
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/business">
            Business Details
            </a>
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/declarations">
            Declarations
            </a>
    </div>

    <br>

    <input type="hidden" id="profile_id" value="{{ $profile->profile_id }}" />

    <div class="ui family-member form">
    {!! Form::open() !!}

    <input type="hidden" name="f_included" id="f_included" value="{{{$f_included or ''}}}">
    <input type="hidden" name="m_included" id="m_included" value="{{{$m_included or ''}}}">
    <input type="hidden" name="save" id="save" value="1">

        <div class="field">
            <div class="father_holder">
                <div class="required field">
                    <label>Details of your Father</label>

                    <div class="two fields">

                        <div class="field">
                            <input type="text" name="father_fam_name" placeholder="Surname/family name" value="{{{ $father['father']->last_name or ''}}}">
                        </div>
                        <div class="field">
                            <input type="text" name="father_first_name" placeholder="First/Given names" value="{{{ $father['father']->first_name or ''}}}">
                        </div>

                    </div>

                    <div class="two fields">

                        <div class="required field">
                            <label>Date of birth</label>
                            @if(isset($father['father']))
                                @include('modules._inline_datebox', $data = ['father_dob', $father['father']->date_of_birth])
                             @else
                                @include('modules._inline_datebox', $data = ['father_dob'])
                             @endif
                        </div>

                        <div class="field">
                            <label>Deseased ?</label>
                            <div class="ui selection dropdown">
                                <input type="hidden" name="father_deseased" id="father_deseased" value="{{{$father['father_ds'] or ''}}}">
                                <i class="dropdown icon"></i>
                                <div class="default text">Deseased ?</div>
                                <div class="menu">
                                  <div class="item" data-value="No">No</div>
                                  <div class="item" data-value="Yes">Yes</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="three fields">
                        <div class="required field">
                            <label>Place of Birth</label>
                            <input type="text" name="father_pob" placeholder="Place of Birth" value="{{{ $father['father']->place_of_birth or ''}}}">
                        </div>
                        <div class="required field">
                            <label>citizenship</label>
                            @if(isset($father['father']))
                                @include('modules._countries_dropdown', $data = ['name'=>'father_citizenship', 'title' => 'Citizenship at birth', 'value' => $father['father_ctzs']])
                            @else
                                @include('modules._countries_dropdown', $data = ['name'=>'father_citizenship', 'title' => 'Citizenship at birth'])
                            @endif
                        </div>
                        <div class="required field">
                            <label>Occupation</label>
                            <input type="text" name="father_occupation" placeholder="Your father's occupation" value="{{{$father['father_occ'] or ''}}}" />
                        </div>
                    </div>

                    <div class="field hide" id="father_res_addr">
                        <label>Father's Current Residential Address</label>
                        <div class="two fields">
                            <div class="field">
                                <input type="text" name="father_street1" placeholder="address line 1" value="{{{$father['father_addr']->street1 or ''}}}">
                            </div>
                            <div class="field">
                                <input type="text" name="father_street2" placeholder="address line 2 (optional)" value="{{{$father['father_addr']->street2 or ''}}}">
                            </div>
                        </div>

                        <div class="three fields">
                            <div class="required field">
                                @if(isset($father['father_addr']))
                                    @include('modules._countries_dropdown', $data = ['name'=>'father_country', 'title' => 'Coutnry of Residence', 'value' => $father['father_addr']->country])
                                @else
                                    @include('modules._countries_dropdown', $data = ['name'=>'father_country', 'title' => 'Coutnry of Residence'])
                                @endif
                            </div>
                            <div class="field">
                                <input type="text" name="father_town" placeholder="town/city" value="{{{$father['father_addr']->town or ''}}}">
                            </div>
                            <div class="field">
                                <input type="text" name="father_post_code" placeholder="postal code" value="{{{$father['father_addr']->postal_code or ''}}}">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="field">
            <div class="mother_holder">
                <div class="required field">
                    <label>Details of your Mother</label>

                    <div class="two fields">

                        <div class="field">
                            <input type="text" name="mother_fam_name" placeholder="Surname/family name" value="{{{ $mother['mother']->last_name or ''}}}">
                        </div>
                        <div class="field">
                            <input type="text" name="mother_first_name" placeholder="First/Given names" value="{{{ $mother['mother']->first_name or ''}}}">
                        </div>

                    </div>

                    <div class="two fields">

                        <div class="required field">
                            <label>Date of birth</label>
                            @if(isset($mother['mother']))
                                @include('modules._inline_datebox', $data = ['mother_dob', $mother['mother']->date_of_birth])
                             @else
                                @include('modules._inline_datebox', $data = ['mother_dob'])
                             @endif
                        </div>

                        <div class="field">
                            <label>Deseased ?</label>
                            <div class="ui selection dropdown">
                                <input type="hidden" name="mother_deseased" id="mother_deseased" value="{{{$mother['mother_ds'] or ''}}}">
                                <i class="dropdown icon"></i>
                                <div class="default text">Deseased ?</div>
                                <div class="menu">
                                  <div class="item" data-value="No">No</div>
                                  <div class="item" data-value="Yes">Yes</div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="three fields">
                        <div class="required field">
                            <label>Place of Birth</label>
                            <input type="text" name="mother_pob" placeholder="Place of Birth" value="{{{ $mother['mother']->place_of_birth or ''}}}">
                        </div>
                        <div class="required field">
                            <label>citizenship</label>
                            @if(isset($mother['mother']))
                                @include('modules._countries_dropdown', $data = ['name'=>'mother_citizenship', 'title' => 'Citizenship at birth', 'value' => $mother['mother_ctzs']])
                            @else
                                @include('modules._countries_dropdown', $data = ['name'=>'mother_citizenship', 'title' => 'Citizenship at birth'])
                            @endif
                        </div>
                        <div class="field">
                            <label>Occupation</label>
                            <input type="text" name="mother_occupation" placeholder="Your mother's occupation" value="{{{$mother['mother_occ'] or ''}}}" />
                        </div>
                    </div>

                    <div class="field hide" id="mother_res_addr">
                        <label>mother's Residential Address</label>
                        <div class="two fields">
                            <div class="field">
                                <input type="text" name="mother_street1" placeholder="address line 1" value="{{{$mother['mother_addr']->street1 or ''}}}">
                            </div>
                            <div class="field">
                                <input type="text" name="mother_street2" placeholder="address line 2 (optional)" value="{{{$mother['mother_addr']->street2 or ''}}}">
                            </div>
                        </div>

                        <div class="three fields">
                            <div class="required field">
                                @if(isset($mother['mother_addr']))
                                    @include('modules._countries_dropdown', $data = ['name'=>'mother_country', 'title' => 'Coutnry of Residence', 'value' => $mother['mother_addr']->country])
                                @else
                                    @include('modules._countries_dropdown', $data = ['name'=>'mother_country', 'title' => 'Coutnry of Residence'])
                                @endif
                            </div>
                            <div class="field">
                                <input type="text" name="mother_town" placeholder="town/city" value="{{{$mother['mother_addr']->town or ''}}}">
                            </div>
                            <div class="field">
                                <input type="text" name="mother_post_code" placeholder="postal code" value="{{{$mother['mother_addr']->postal_code or ''}}}">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="field">
            <label>Provide Details of you family in-law's not included in your application (include mother, father in-law)</label>

            <div class="ui middle aligned inlaw selection list">
                @if(!empty($inlaws))
                    @foreach($inlaws as $i)

                        <div class="item {{ $i['family_member_id'] }}">
                            @if($i['type'] !== 'Profile')
                                <div class="right floated content">
                                    <div class="ui blue inverted inlaw edit button" id="{{ $i['family_member_id'] }}">Edit</div>
                                    <div class="ui red inverted inlaw delete button" id="{{ $i['family_member_id'] }}">Delete</div>
                                </div>
                            @endif
                            <i class="ui avatar image user icon"></i>
                            <div class="content">
                                {{$i['fullname']}} ({{$i['relationship']}})
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <a href="#" id="add_inlaw_link"><i class="add circle icon"></i> Click here to add family in-law</a>
        </div>

        <div class="field">
            <label>Provide details of you brothers and sisters (include half, step and adopted)</label>

            <div class="ui middle aligned sibling selection list">
                @if(!empty($siblings))
                    @foreach($siblings as $sibling)

                        <div class="item {{ $sibling->family_member_id }}">
                            <div class="right floated content">
                                <div class="ui blue inverted sibling edit button" id="{{ $sibling->family_member_id }}">Edit</div>
                                <div class="ui red inverted sibling delete button" id="{{ $sibling->family_member_id }}">Delete</div>
                            </div>
                            <i class="ui avatar image user icon"></i>
                            <div class="content">
                                {{$sibling->first_name}} {{$sibling->last_name}} ({{$sibling->relationship}})
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <a href="#" id="add_sibling_link"><i class="add circle icon"></i> Click here to add Sibling</a>
        </div>

        <div class="field">
            <label>Provide Details of any ex-spouses</label>

            <div class="ui middle aligned previous_spouse selection list">
                @if(!empty($ex))
                    @foreach($ex as $e)

                        <div class="item {{ $e->previous_spouse_id }}">
                            <div class="right floated content">
                                <div class="ui blue inverted previous_spouse edit button" id="{{ $e->previous_spouse_id }}">Edit</div>
                                <div class="ui red inverted previous_spouse delete button" id="{{ $e->previous_spouse_id }}">Delete</div>
                            </div>
                            <i class="ui avatar image user icon"></i>
                            <div class="content">
                                {{ $e->full_name }} (Ex-Spouse)
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <a href="#" id="add_previous_spouse_link"><i class="add circle icon"></i> Click here to add family ex-spouses</a>
        </div>

        <div class="field">
            <label>Details of children not included in application (biological, adopted and step-children)</label>

            <div class="ui middle aligned child selection list">
                @if(!empty($children))
                    @foreach($children as $c)

                        <div class="item {{ $c->family_member_id }}">
                            <div class="right floated content">
                                <div class="ui blue inverted child edit button" id="{{ $c->family_member_id }}">Edit</div>
                                <div class="ui red inverted child delete button" id="{{ $c->family_member_id }}">Delete</div>
                            </div>
                            <i class="ui avatar image user icon"></i>
                            <div class="content">
                                {{$c->first_name}} {{$c->last_name}} ({{$c->relationship}})
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <a href="#" id="add_child_link"><i class="add circle icon"></i> Click here to add child</a>
        </div>

        <div class="two fields">
            <div class="field">
                <a class="ui fluid blue button" href="{{Request::url()}}">Refresh</a>
            </div>
            <div class="field">
                <button class="ui save fluid blue button">Save</button>
            </div>
        </div>

    {!! Form::close() !!}
    </div>

</div>

@stop

@section('modal')
    @include('modals._add_previous_spouse')
    @include('modals._add_sibling')
    @include('modals._add_in_law')
    @include('modals._add_child')
@stop

@section('script')
    <script src="{{ URL::asset('js/cases_family_members.js') }}"></script>
    <script src="{{ URL::asset('js/sibling.js') }}"></script>
    <script src="{{ URL::asset('js/previous_spouse.js') }}"></script>
    <script src="{{ URL::asset('js/in_laws.js') }}"></script>
    <script src="{{ URL::asset('js/child.js') }}"></script>
@stop
