@extends('nladminpanel::app_left')

@section('title', 'Personal Data')

@section('left')
    @include('nladminpanel::_left', $data = ['link' => 'data'])
@stop

@section('content')

<h3 class="ui center aligned block header">{{$profile->first_name}} {{$profile->last_name}} - Personal Data</h3>

@if($user->profiles()->count() > 4)
    @include('nladminpanel::_dep_guide_split', $d = ['page' => 'personal-data'])
@else
    @include('nladminpanel::_dep_guide', $d = ['page' => 'personal-data'])
@endif

<div class="ui stacked segment">

    <div class="ui top attached tabular menu">
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/important-information">
            Important Information
            </a>
            <a class="active item" href="/admin/profile/{{$profile->profile_id}}/application/personal-data">
            Perosnal Details
            </a>
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/medical-details">
            Medical Details
            </a>
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/family-members">
            Family Details
            </a>
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/business">
            Business Details
            </a>
            <a class="item" href="/admin/profile/{{$profile->profile_id}}/application/declarations">
            Declarations
            </a>
    </div>

    <br>

    <input type="hidden" id="profile_id" value="{{ $profile->profile_id }}" />
    <input type="hidden" id="res_counter" value="{{$profile->addresses()->where('address_type', 'res')->count()}}" />
    <input type="hidden" id="military" value="{{($milInfo) ? '1' : '0'}}" />
    <input type="hidden" id="offence" value="{{(!empty($milInfo) and $milInfo->offence) ? '1' : '0'}}" />

    <div class="ui personal-data form">
    {!! Form::open() !!}

        <input type="hidden" name="save" id="save" value="1" />

        @include('cases.input_personal_details')

        <div class="two fields">
            <div class="field">
                <a class="ui fluid blue button" href="{{Request::url()}}">Refresh</a>
            </div>
            <div class="field">
                <button class="ui save fluid blue button">Save</button>
            </div>
        </div>
    {!! Form::close() !!}
    </div>

</div>

@stop

@section('modal')
    @include('modals._add_passport')
    @include('modals._add_address')
    @include('modals._add_qualification')
@stop

@section('script')
    <script src="{{ URL::asset('js/cases_personal_data.js') }}"></script>
    <script src="{{ URL::asset('js/cases_personal_data_addresses.js') }}"></script>
    <script src="{{ URL::asset('js/cases_personal_data_qualification.js') }}"></script>
    <script src="{{ URL::asset('js/cases_antigua_passport.js') }}"></script>
@stop
