<div class="ui visible left demo vertical inverted sidebar labeled icon menu">
  <a class="{{($data['link'] === 'data') ? 'active' : ''}} item" href="/admin/profile/{{$profile->profile_id}}/application/important-information">
    <i class="edit icon"></i>
    Applicant Data
  </a>
  <a class="{{($data['link'] === 'gov') ? 'active' : ''}} item" href="/admin/profile/{{$profile->profile_id}}/application/government-docs">
    <i class="file pdf outline icon"></i>
    Government Docs
  </a>
  <a class="{{($data['link'] === 'doc') ? 'active' : ''}} item" href="/users/filesystem/list-documents/{{$profile->profile_id}}">
    <i class="file pdf outline icon"></i>
    Applicant Docs
  </a>
  <a class="{{($data['link'] === 'fin') ? 'active' : ''}} item" href="/admin/profile/{{$profile->profile_id}}/application/finance">
    <i class="edit icon"></i>
    Finance
  </a>
  <a class="{{($data['link'] === 'message') ? 'active' : ''}} item" href="/admin/messaging/user-messages/{{$profile->profile_id}}">
    <i class="mail outline icon"></i>
    Messages
  </a>
  <a class="{{($data['link'] === 'approval') ? 'active' : ''}} item" href="/admin/profile/{{$profile->profile_id}}/application/approvals">
    <i class="checkmark icon"></i>
    Approvals
  </a>
</div>
