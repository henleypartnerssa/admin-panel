@extends('nladminpanel::app_left')

@section('title', 'Application Finance')

@section('left')
    @include('nladminpanel::_left', $data = ['link' => 'fin'])
@stop

@section('content')

<h3 class="ui center aligned block header">{{$profile->first_name}} {{$profile->last_name}} - Application Finance</h3>

<div class="ui stacked segment">

    <div id="file_inputs">
    </div>

    <input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />
    <input type="hidden" id="profile_id" value="{{ $profile->profile_id }}" />

    <div class="ui grid">
        <div class="row">

            <div class="ten wide column">
                <div class="ui form">
                    <div class="field">
                        <input type="hidden" id="total_request_service" style="display: none" name="total_request_service" value="{{{count($files['payment_request_service_fees']) or '0'}}}" />
                        <label id="requests">Application Request for Payment - Service Fees</label>
                        <div class="ui middle aligned request_service selection list">
                            @if($files)
                                @foreach($files['payment_request_service_fees'] as $file)
                                <div class="item {{$file['key']}}">
                                    <div class="right floated content">
                                        <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                                    </div>
                                    <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                                    <div class="content">File Name: {{$file['file']}}</div>
                                </div>
                                @endforeach
                            @endif
                        </div>
                        <a href="#" id="add_request_service">
                            <i class="add upload icon"></i>Click here to upload Request for Payment - Service Fees
                        </a>
                    </div>
                </div>
                <br>
                <div class="field">
                    <input type="hidden" id="total_request_gov" style="display: none" name="total_request_gov" value="{{{count($files['payment_request_government_fees']) or '0'}}}" />
                    <label id="requests">Application Request for Payment - Government Investment Fees</label>
                    <div class="ui middle aligned request_gov selection list">
                        @if($files)
                            @foreach($files['payment_request_government_fees'] as $file)
                            <div class="item {{$file['key']}}">
                                <div class="right floated content">
                                    <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                                </div>
                                <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                                <div class="content">File Name: {{$file['file']}}</div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                    <a href="#" id="add_request_gov">
                        <i class="add upload icon"></i>Click here to upload Request for Payment - Government Investment Fees
                    </a>
                </div>
                <br>
                <div class="field">
                    <input type="hidden" id="total_receipt_service" style="display: none" name="total_receipt_service" value="{{{count($files['service_fees_receipt']) or '0'}}}" />
                    <label id="requests">Receipt - Service Fees</label>
                    <div class="ui middle aligned receipt_service selection list">
                        @if($files)
                            @foreach($files['service_fees_receipt'] as $file)
                            <div class="item {{$file['key']}}">
                                <div class="right floated content">
                                    <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                                </div>
                                <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                                <div class="content">File Name: {{$file['file']}}</div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                    <a href="#" id="add_receipt_service">
                        <i class="add upload icon"></i>Click here to upload Receipt for Service Fees
                    </a>
                </div>
                <br>
                <div class="field">
                    <input type="hidden" id="total_receipt_gov" style="display: none" name="total_receipt_gov" value="{{{count($files['government_fees_receipt']) or '0'}}}" />
                    <label id="requests">Receipt - Government Fees</label>
                    <div class="ui middle aligned receipt_gov selection list">
                        @if($files)
                            @foreach($files['government_fees_receipt'] as $file)
                            <div class="item {{$file['key']}}">
                                <div class="right floated content">
                                    <div class="ui green delete button" id="{{$file['key']}}" data-fileName="{{$file['file']}}">Delete</div>
                                </div>
                                <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                                <div class="content">File Name: {{$file['file']}}</div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                    <a href="#" id="add_receipt_gov">
                        <i class="add upload icon"></i>Click here to upload Receipt for Government Fees
                    </a>
                </div>
            </div>

            <div class="six wide column">
                <div class="ui card" style="float:right">
                    <div class="content">
                        <div class="header">Account Balance</div>
                    </div>
                    <div class="content">
                            <i class="huge dollar icon"></i> <b><label id="account_total" style="font-size:20px">{{{ $profile->application->account_balance or '0'}}}</label></b>
                    </div>
                    <div class="extra content">
                        <button class="ui fluid inverted blue change button">Change Total</button>
                    </div>
                </div>
                <div class="ui change form" style="float:right;display:none">
                    <div class="two fields">
                        <div class="ten wide field">
                            <input type="text" name="total" value="{{{ $profile->application->account_balance or '0'}}}">
                        </div>
                        <div class="six wide field">
                            <div class="ui fluid inverted green change_submit button">change</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@stop

@section('modal')

@stop

@section('script')
    <script src="{{ URL::asset('js/file_upload.js') }}"></script>
    <script src="{{ URL::asset('js/file_manager.js') }}"></script>
    <script src="{{ URL::asset('js/admin_finance.js') }}"></script>
@stop
