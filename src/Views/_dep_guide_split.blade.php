<div class="ui grid">
    <div class="row">
        <div class="eight wide column">
            <div class="ui middle aligned selection list">
                @foreach($user->profiles()->orderBy('created_at')->get() as $i => $p)
                @if ($i <= ($user->profiles()->count() / 2))
                <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/admin/profile/{{$p->profile_id}}/application/{{$d['page']}}">
                    <div class="right floated content"></div>
                    <i class="ui avatar image user icon"></i>
                    <div class="content">
                        {{$p->first_name}} {{$p->last_name}} ({{$p->belongs_to}})
                    </div>
                </a>
                @endif
                @endforeach
            </div>
        </div>
        <div class="eight wide column">
            <div class="ui middle aligned selection list">
                @foreach($user->profiles()->orderBy('created_at')->get() as $i => $p)
                @if ($i > ($user->profiles()->count() / 2))
                <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/admin/profile/{{$p->profile_id}}/application/{{$d['page']}}">
                    <div class="right floated content"></div>
                    <i class="ui avatar image user icon"></i>
                    <div class="content">
                        {{$p->first_name}} {{$p->last_name}} ({{$p->belongs_to}})
                    </div>
                </a>
                @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
