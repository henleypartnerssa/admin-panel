@extends('nladminpanel::app_left')

@section('title', 'Government Documents')

@section('left')
    @include('nladminpanel::_left', $data = ['link' => 'gov'])
@stop

@section('content')

<h3 class="ui center aligned block header">{{$profile->first_name}} {{$profile->last_name}} - Government Documents</h3>

    <div class="ui stacked segment">

        <div class="ui grid">
            <div class="row">
                <div class="sixteen wide column">
                    <div class="ui tabular menu">
                        @foreach($user->profiles()->orderBy('created_at')->get() as $p)
                        <a class="item {{($p->profile_id === $profile->profile_id) ? 'active' : ''}}" href="/admin/profile/{{$p->profile_id}}/application/government-docs">
                            <div class="content">
                                {{$p->first_name}} {{$p->last_name}}
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="ui form">
            <div class="field">
                <div class="ui middle aligned download selection list" data-token='{{ csrf_token() }}'>
                    <!--FORM D1-->
                    <div class="item" id="D1">
                        <div class="right floated D1 content">
                            <div class="ui D1 generate button" data-profile="{{$profile->profile_id}}" data-form="D1">Generate PDF</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">D1 - Disclosure</div>
                    </div>

                    <!--FORM D2-->
                    <div class="item" id="D2">
                        <div class="right floated D2 content">
                            <div class="ui D2 generate button" data-profile="{{$profile->profile_id}}" data-form="D2">Generate PDF</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">D2 - Fingerprint &amp; Signature</div>
                    </div>

                    <!--FORM D3-->
                    <div class="item" id="D3">
                        <div class="right floated D3 content">
                            <div class="ui D3 generate button" data-profile="{{$profile->profile_id}}" data-form="D3">Generate PDF</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">D3 - Medical Questionnaire</div>
                    </div>

                    @if($profile->belongs_to === 'Principal Applicant')
                        <!--FORM D4-->
                        <div class="item" id="D4">
                            <div class="right floated D4 content">
                                <div class="ui D4 generate button" data-profile="{{$profile->profile_id}}" data-form="D4">Generate PDF</div>
                            </div>
                            <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                            <div class="content">D4 - Investment Agreement</div>
                        </div>
                    @endif

                    <!--Passport-->
                    <div class="item" id="Passport">
                        <div class="right floated Passport content">
                            <div class="ui Passport generate button" data-profile="{{$profile->profile_id}}" data-form="Passport">Generate PDF</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">DOM - Passport Application</div>
                    </div>

                    <!--FORM 12-->
                    <!--div class="item" id="Form12">
                        <div class="right floated Form12 content">
                            <div class="ui Form12 generate button" data-profile="{{$profile->profile_id}}" data-form="Form12">Generate PDF</div>
                        </div>
                        <i class="ui avatar image"><i class="red large file pdf outline icon"></i></i>
                        <div class="content">Form 12</div>
                    </div-->
                </div>
            </div>

            <div class="field">
                <div class="ui error message" style="display:none">
                    <div class="header">Error Generating File</div>
                    <p>An Error occurred while generating the governemnt form. Please try again later.</p>
                </div>
            </div>

        </div>
        <div class="bottom"></div>

    </div>

@stop

@section('script')
    <script src="{{ URL::asset('js/admin_gov_docs.js') }}"></script>
@stop
