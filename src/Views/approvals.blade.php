@extends('nladminpanel::app_left')

@section('title', 'Government Documents')

@section('left')
    @include('nladminpanel::_left', $data = ['link' => 'approval'])
@stop

@section('content')
    <div class="ui container">
        <h3 class="ui center aligned block header">{{$profile->first_name}} {{$profile->last_name}} - Government Documents</h3>

        <div class="ui bottom attached segment">

            <table class="ui compact celled definition table">
                <thead>
                    <tr>
                        <th></th>
                        <th>Appl. Step No</th>
                        <th>Description</th>
                        <th>Approved</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="dd" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>3</td>
                        <td>Key Infrmation - Due Diligence Check</td>
                        @if($app->dd_check)
                            <td class="center aligned" id="dd"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="dd">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="retainer" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>4</td>
                        <td>Retainer Received</td>
                        @if($app->retainer_paid)
                            <td class="center aligned" id="retainer"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="retainer">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="dr" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>7</td>
                        <td>Data Review</td>
                        @if($app->data_review)
                            <td class="center aligned" id="dr"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="dr">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="balance" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>10</td>
                        <td>Balance of Investment Fees Paid</td>
                        @if($app->balance_paid)
                            <td class="center aligned" id="balance"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="balance">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="gov_docs" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>11</td>
                        <td>Received Government Documents</td>
                        @if($app->docs_received)
                            <td class="center aligned" id="gov_docs"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="gov_docs">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="review" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>12</td>
                        <td>Package Reviewed</td>
                        @if($app->application_review)
                            <td class="center aligned" id="review"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="review">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="dominica" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td><b>Internal</b></td>
                        <td>Package Sent to Dominica</td>
                        @if($app->sent_to_dominica)
                            <td class="center aligned" id="dominica"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="dominica">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="submitted" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td><b>Internal</b></td>
                        <td>Pacakge Submitted to Government</td>
                        @if($app->submitted_to_gov)
                            <td class="center aligned" id="submitted"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="submitted">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="ciu" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>13</td>
                        <td>Received CIU Approval</td>
                        @if($app->ciu_approval)
                            <td class="center aligned" id="ciu"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="ciu">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="investment" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>14</td>
                        <td>Government Investment Complete</td>
                        @if($app->investment_completed)
                            <td class="center aligned" id="investment"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="investment">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="oath" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>15</td>
                        <td>Received Oath Confirmation</td>
                        @if($app->oath)
                            <td class="center aligned" id="oath"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="oath">No</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="collapsing">
                            <div class="ui slider checkbox">
                                <input name="throughput" id="passport" type="radio">
                                <label></label>
                            </div>
                        </td>
                        <td>16</td>
                        <td>Passport Issued</td>
                        @if($app->passport_issued)
                            <td class="center aligned" id="passport"><i class="large green checkmark icon"></i></td>
                        @else
                            <td class="center aligned error" id="passport">No</td>
                        @endif
                    </tr>
                </tbody>
                <tfoot class="full-width">
                    <tr>
                        <td></td>
                        <th colspan="4">
                            <div class="ui right floated small primary button disabled" id="edit-email">
                              Edit Response Email
                            </div>
                            <div class="ui small inverted blue approve button disabled">
                                Approve
                            </div>
                            <div class="ui small inverted orange reverse button disabled">
                                Reverse Approve
                            </div>
                            <div class="ui small inverted green mail button disabled">
                                Send Response Email
                            </div>
                            <div class="ui small red bypass button disabled hidden">
                                BYPASS
                            </div>
                        </th>
                    </tr>
                </tfoot>
            </table>

            <div class="ui message form" style="display:none">
                <div class="field">
                    <textarea name="custom-message" id="custom-message"></textarea>
                </div>
                <div class="three fields">
                    <div class="field">
                    </div>
                    <div class="field">
                    </div>
                    <div class="field">
                        <button class="ui save-custom-message fluid blue right labeled icon button">
                            Save Changes
                            <i class="large angle right icon"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!--Warining Box for Under-->
            <div class="ui warning under message hidden">
              <i class="close icon"></i>
              <div class="header">
                <i class="warning sign icon"></i>
                Please Note
              </div>
              The Application is currently at a step below your approval selection.
              If you wish to ignore this warning and commence with the approval then
              please do so by clicking the BYPASS. Please note
              that if you click the bypass button, the applcation process will
              commence beyond you selected approval.
            </div>

            <!--Warining Box for Over-->
            <div class="ui warning over message hidden">
              <i class="close icon"></i>
              <div class="header">
                <i class="warining icon"></i>
                Please Note
              </div>
              The Application is currently at a step above your approval selection.
              If you wish to ignore this warning and commence with the approval then
              please do so by clicking the BYPASS button listed below. Please note
              that if you click the bypass button, the applcation process will
              rollback to the point of your approval selection.
            </div>

            <div class="ui green approval message hidden">Approval successfully set.</div>
            <div class="ui green sent message hidden">Email successfully sent.</div>
            <div class="ui green rollback message hidden">Approval successfully rolled back.</div>

            <div class="ui dd-error negative message hidden">
              <i class="close icon"></i>
              <div class="header">
                  Error Sending Response Email
              </div>
              <span>Please make sure you have attached the following documents</span>
              <ul>
                  <li>Request for Payment - Service Fees</li>
              </ul>
            </div>

            <div class="ui retainer-error negative message hidden">
              <i class="close icon"></i>
              <div class="header">
                  Error Sending Response Email
              </div>
              <span>Please make sure you have attached the following documents</span>
              <ul>
                  <li>Service Fees - Receipt</li>
              </ul>
            </div>

            <div class="ui dr-error negative message hidden">
              <i class="close icon"></i>
              <div class="header">
                  Error Sending Response Email
              </div>
              <span>Please make sure you have attached the following documents</span>
              <ul>
                  <li>Request for Payment - Government Fees</li>
              </ul>
            </div>

            <div class="ui balance-error negative message hidden">
              <i class="close icon"></i>
              <div class="header">
                  Error Sending Response Email
              </div>
              <span>Please make sure you have attached the following documents</span>
              <ul>
                  <li>Government Fees - Receipt</li>
              </ul>
            </div>

            <div class="ui ciu-error negative message hidden">
              <i class="close icon"></i>
              <div class="header">
                  Error Sending Response Email
              </div>
              <span>Please make sure you have attached the following documents</span>
              <ul>
                  <li>Government Approval Letter</li>
              </ul>
            </div>

        </div>

        <input type="hidden" id="token" style="display: none" value="{{ csrf_token() }}" />
        <input type="hidden" id="profile_id" style="display: none" value="{{ $profile->profile_id }}" />

        <input type="hidden" class="dd" style="display: none" value="{{{$app->dd_check or '0'}}}" />
        <input type="hidden" class="retainer" style="display: none" value="{{{$app->retainer_paid or '0'}}}" />
        <input type="hidden" class="dr" style="display: none" value="{{{$app->data_review or '0'}}}" />
        <input type="hidden" class="balance" style="display: none" value="{{{$app->balance_paid or '0'}}}" />
        <input type="hidden" class="gov_docs" style="display: none" value="{{{$app->docs_received or '0'}}}" />
        <input type="hidden" class="review" style="display: none" value="{{{$app->application_review or '0'}}}" />
        <input type="hidden" class="dominica" style="display: none" value="{{{$app->sent_to_dominica or '0'}}}" />
        <input type="hidden" class="submitted" style="display: none" value="{{{$app->submitted_to_gov or '0'}}}" />
        <input type="hidden" class="ciu" style="display: none" value="{{{$app->ciu_approval or '0'}}}" />
        <input type="hidden" class="investment" style="display: none" value="{{{$app->investment_completed or '0'}}}" />
        <input type="hidden" class="oath" style="display: none" value="{{{$app->oath or '0'}}}" />
        <input type="hidden" class="passport" style="display: none" value="{{{$app->passport_issued or '0'}}}" />
@stop

@section('script')
    <script src="{{ URL::asset('js/admin_approvals.js') }}"></script>
@stop
