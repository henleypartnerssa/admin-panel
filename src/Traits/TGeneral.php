<?php

namespace hpsadev\NLAdminPanel\Traits;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Group;
use App\User;
use App\Profile;
use App\Application;
use hpsadev\NLAdminPanel\NLAdminPanel;
use hpsadev\ProgramsFileManager\FileManager;
use hpsadev\Mailer\Mailer;
use App\Util\SMS;

/**
 * This Package Trait contains general Controller Methods.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.2
 */
trait TGeneral {

    public function adminLogin(Request $request) {
        if (NLAdminPanel::validateAdminUser()) {
            $auth = Auth::attempt(['username' => $request['username'], 'password' => $request['password']]);
            if ($auth) {
                $user   = Auth::user();
                $group  = Group::where('roles', 'admin')->get()->first();
                if ($user->pin === ((int)$request['otp']) && $user->group_id === $group->group_id) {
                    return '1';
                } else {
                    $this->logoutAdmin();
                    return '0';
                }
            } else {
                $this->logoutAdmin();
                return '0';
            }
        }
    }

    public function logoutAdmin() {
        Auth::logout();
        return redirect('/admin');
    }

    public function sendPin(Request $request) {
        $auth = Auth::attempt(['username' => $request['username'], 'password' => $request['password']]);
        if ($auth) {
            $user   = Auth::user();
            $group  = Group::where('roles', 'admin')->get()->first();
            $sms = new SMS($user);
            if ($sms->sendPin()) {
                Auth::logout();
                return '1';
            } else {
                Auth::logout();
                return '0';
            }
        }
    }

    public function adminDashboard() {
        $apps = Application::all();
        return view('nladminpanel::dashboard', compact('apps'));
    }

    public function financeView($profileID) {
        $user       = Profile::find($profileID)->user;
        $profile    = $user->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $fm         = new FileManager($profile->profile_id);
        $files      = $fm->getAllProfileFiles(['payments', 'receipts']);
        return view('nladminpanel::finance', compact('profile', 'files'));
    }

    public function updateAccBalance($profileID, Request $request) {
        $profile    = Profile::find($profileID);
        $app        = $profile->application;
        $app->account_balance = $request['total'];
        $app->save();
        return $app->account_balance;
    }

    public function governmentDocs($profileID) {
        $profile    = Profile::find($profileID);
        $user       = $profile->user;
        return view('nladminpanel::gov_docs', compact('profile', 'user'));
    }

    public function approvals($profileID) {
        $user       = Profile::find($profileID)->user;
        $profile    = $user->profiles()->where('belongs_to', 'Principal Applicant')->first();
        $app        = $profile->application;
        return view('nladminpanel::approvals', compact('profile', 'user', 'app'));
    }

    public function updateState($profileID, Request $request) {
        $validation = NLAdminPanel::validateState($profileID, $request['approval']);
        if ($validation === '1') {
            NLAdminPanel::updateState($profileID,$request);
            return '1';
        } else {
            if ($request['override'] === '1') {
                NLAdminPanel::updateState($profileID,$request);
                return '1';
            } else {
                return $validation;
            }
        }
    }

    /**
     * Send Email Controller Function.
     *
     * @since 0.0.2 Function Init
     * @since 0.0.3 updated function to not only 1
     *        		but 0 when mail error.
     *
     * @param  integer  $profileID  Profile ID
     * @param  Request  $request    POST data
     * @return integer              indicator
     */
    public function sendEmail($profileID, Request $request) {
        $profile    = Profile::find($profileID);
        $user       = $profile->user;
        $approval   = $request['approval'];
        $fm         = new FileManager($profileID);
        if ($request['custom'] === 'true') {
            $sent = Mailer::customEmail([
                'username'      => $user->username,
                'raw'           => $request['raw'],
                'attachements'  => Mailer::lkpAttachemnts($approval,$fm, $profile),
            ]);
            return $sent;
        } else {
            $sent = Mailer::stepApproval([
                'user' => $user,
                'data' => [
                    'title'     => $profile->title,
                    'name'      => $profile->first_name,
                    'surname'   => $profile->last_name
                ],
                'view'          => Mailer::lkpView($approval),
                'attachements'  => Mailer::lkpAttachemnts($approval,$fm, $profile),
            ]);
            return $sent;
        }
    }

}
