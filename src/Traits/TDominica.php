<?php

namespace hpsadev\NLAdminPanel\Traits;

use Illuminate\Http\Request;
use App\Http\Requests;
use hpsadev\dominicadirect\Dominica;

/**
 * This Package Trait contains Dominica Specific Controller Methods.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.2
 */
trait TDominica {

    public function importnantInfoView($profileId) {
        $domStep = new Dominica($profileId, true);
        return $domStep->importnantInfoView($profileId, 'nladminpanel::Dominica.important_info');
    }

    public function personalDataView($profileID) {
        $domStep = new Dominica($profileID, true);
        return $domStep->personalDataView('nladminpanel::Dominica.personal_data');
    }

    public function medicalDetailsView($profileID) {
        $domStep = new Dominica($profileID, true);
        return $domStep->medicalDetailsView('nladminpanel::Dominica.medical');
    }

    public function familyMembersView($profileID) {
        $domStep = new Dominica($profileID, true);
        return $domStep->familyMembersView('nladminpanel::Dominica.family_members');
    }

    public function businessView($profileID) {
        $domStep = new Dominica($profileID, true);
        return $domStep->businessView('nladminpanel::Dominica.business');
    }

    public function declarationsView($profileID) {
        $domStep = new Dominica($profileID, true);
        return $domStep->declarationsView('nladminpanel::Dominica.declarations');
    }

    public function importnantInfoPost($profileID, Request $request) {
        $domStep = new Dominica($profileID, true);
        return $domStep->importnantInfoRedirect($request);
    }

    public function personalDataPost($profileID, Request $request) {
        $domStep = new Dominica($profileID, true);
        return $domStep->personalDataRedirect($request);
    }

    public function medicalDetailsPost($profileID, Request $request) {
        $domStep = new Dominica($profileID, true);
        return $domStep->medicalDetailsRedirect($request);
    }

    public function familyMembersPost($profileID, Request $request) {
        $domStep = new Dominica($profileID, true);
        return $domStep->familyMembersRedirect($request);
    }

    public function businessPost($profileID, Request $request) {
        $domStep = new Dominica($profileID, true);
        return $domStep->businessRedirect($request);
    }

    public function declarationsPost($profileID, Request $request) {
        $domStep = new Dominica($profileID, true);
        return $domStep->declarationsRedirect($request);
    }

}
