<?php

namespace hpsadev\NLAdminPanel\Traits;

/**
 * This Package Trait groups all Traits.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.2
 */
trait TControllerMethods {
    use TGeneral;
    use TDominica;
}
