$('document').ready(function() {

    var docs = ['D1', 'D2', 'D3', 'D4', 'Passport', 'Form 12'];

    $('document').ready(function() {
        $('html, body').animate({
       scrollTop: $('.bottom').offset().top
    }, 'slow');
    });

    $.each(docs, function(key, value) {
        $('.ui.'+value+'.button').on('click', function() {
            $('.ui.error.message').fadeOut('slow');
            var profile = $(this).data('profile');
            var form    = $(this).data('form');
            $('.ui.'+value+'.button').addClass('loading');
            generatePdfFile(profile, value, form);
        });
    });

    function generatePdfFile(profile, type, form) {
        $.ajax({
            type    : 'POST',
            url     : '/users/filesystem/generate-government-form',
            async   : true,
            data    : {
                _token      : $('.ui.download.list').data('token'),
                form        : form,
                profile_id  : profile
            },
            success: function(data) {
                var json = JSON.parse(data);
                if (json.state === '1') {
                    $('.ui.'+type+'.button').removeClass('loading');
                    $('.ui.'+type+'.button').addClass('disabled');
                    $('.right.floated.'+type+'.content').append(downloadLink(type, json.file, profile));
                } else {
                    $('.ui.'+type+'.button').removeClass('loading');
                    $('.ui.error.message').fadeIn('slow');
                }
            }
        });
    }

    function downloadLink(type, file, profileId) {
        var html = '<a class="ui green '+type+' generate button" href="/users/filesystem/download-profile-downloads/'+profileId+'-'+file+'">Download PDF</a>';
        return html;
    }

});
