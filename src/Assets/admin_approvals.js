$('document').ready(function() {

    var checkBoxEl      = $('.ui.slider.checkbox');
    var editmailBtnEl   = $('#edit-email');
    var sendmailBtnEl   = $('.ui.mail.button');
    var approveBtnEl    = $('.ui.approve.button');
    var reverseBtnEl    = $('.ui.reverse.button');
    var bypassBtnEl     = $('.ui.bypass.button');
    var underWarnEl     = $('.ui.warning.under');
    var overWarnEl      = $('.ui.warning.over');
    var apprMsg         = $('.ui.approval.message');
    var emailMsg        = $('.ui.sent.message');
    var rollbckMsg      = $('.ui.rollback.message');
    var custMailForm    = $('.ui.message.form');
    var custMailSaveBtn = $('.ui.save-custom-message.button');

    var selected    = null;
    var approval    = null;
    var reverse     = false;
    var custom      = false;

    // on slide button active
    checkBoxEl.checkbox({
        onChecked: function() {
            var id      = $(this).attr('id');
            selected    = id;
            approval    = $('.'+selected).val();
            btnSettings();
        }
    });

    // on appove button click
    approveBtnEl.on('click', function() {
        $(this).addClass('loading');
        setState(0, this);
        reverse = false;
    });

    // on rollback button click
    reverseBtnEl.on('click', function() {
        $(this).addClass('loading');
        reverse = true;
        setState(0, this);
    });

    // on bypass button click
    bypassBtnEl.on('click', function() {
        $(this).addClass('loading');
        underWarnEl.hide('slow');
        overWarnEl.hide('slow');
        setState(1, this);
        reverse = false;
    });

    // on send Email button click
    sendmailBtnEl.on('click', function() {
        $(this).addClass('loading');
        sendEmail();
    });

    // on edit email button click
    editmailBtnEl.on('click', function() {
        custMailForm.show('slow');
    });

    // on save custom message buttom click
    custMailSaveBtn.on('click', function() {
        custom = true;
        custMailForm.hide('slow');
    });

    // internal function that enabled and disabled button on approval.
    function btnSettings() {
        if (approval === '1') {
            reverseBtnEl.removeClass('disabled');
            sendmailBtnEl.removeClass('disabled');
            approveBtnEl.addClass('disabled');
            editmailBtnEl.addClass('disabled');
        } else {
            reverseBtnEl.addClass('disabled');
            approveBtnEl.removeClass('disabled');
            editmailBtnEl.removeClass('disabled');
            sendmailBtnEl.addClass('disabled');
        }
    }

    // Executes Set App state Request.
    function setState(override, btn) {
        profileId = $('#profile_id').val();
        $.ajax({
            type:   'POST',
            async:  true,
            url:    '/admin/profile/'+profileId+'/application/update-state',
            data:   {
                _token:     $('#token').val(),
                approval:   selected,
                override:   override,
                rollback:   reverse
            },
            success: function(data) {
                if (data === '-2') {
                    rmLdFromBtns();
                    underWarnEl.show('slow');
                    bypassBtnEl.removeClass('disabled');
                    // greater than
                } else if (data === '-1') {
                    rmLdFromBtns();
                    overWarnEl.show('slow');
                    bypassBtnEl.removeClass('disabled');
                    // successfull update
                } else if (data === '1') {
                    rmLdFromBtns();
                    bypassBtnEl.addClass('disabled');
                    if (reverse) {
                        $('td#'+selected).replaceWith('<td class="center aligned error" id="'+selected+'">No</td>');
                        reverseBtnEl.addClass('disabled');
                        sendmailBtnEl.addClass('disabled');
                        approveBtnEl.removeClass('disabled');
                    } else {
                        $('td#'+selected).replaceWith('<td class="center aligned" id="'+selected+'"><i class="large green checkmark icon"></i></td>');
                        reverseBtnEl.removeClass('disabled');
                        sendmailBtnEl.removeClass('disabled');
                        approveBtnEl.addClass('disabled');
                    }

                    apprMsg.show('slow');
                    setTimeout(function() {
                        apprMsg.hide('slow');
                    }, 3000);
                }
            }
        });
    }

    function sendEmail() {
        profileId = $('#profile_id').val();
        rData = $('#custom-message').val();
        r = rData.replace(/\n/g, "\\\\n");
        console.log(r);
        $.ajax({
            type:   'POST',
            async:  true,
            url:    '/admin/profile/'+profileId+'/application/send-email',
            data:   {
                _token:     $('#token').val(),
                approval:   selected,
                custom:     custom,
                raw:        r
            },
            success: function(data) {
                if (data === '1') {
                    sendmailBtnEl.removeClass('loading');
                    emailMsg.show('slow');
                    setTimeout(function() {
                        emailMsg.hide('slow');
                    }, 3000);
                } else {
                    sendmailBtnEl.removeClass('loading');
                    $('.ui.'+selected+'-error.negative.message').show('slow');
                }
            }
        });
    }

    // remove loading class from approve buttons
    function rmLdFromBtns() {
        editmailBtnEl.removeClass('loading');
        sendmailBtnEl.removeClass('loading');
        approveBtnEl.removeClass('loading');
        reverseBtnEl.removeClass('loading');
        bypassBtnEl.removeClass('loading');
    }

    function disableAll() {
        editmailBtnEl.addClass('disabled');
        sendmailBtnEl.addClass('disabled');
        approveBtnEl.addClass('disabled');
        reverseBtnEl.addClass('disabled');
        bypassBtnEl.addClass('disabled');
        checkBoxEl.checkbox('uncheck');
        $('.'+selected).val('0');
    }

});
