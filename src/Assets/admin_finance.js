$('document').ready(function() {

    var reqServiceUpload = new FileUploader("Request for Payment - Service Fees","request_service", "payments");
    var reqGoverneUpload = new FileUploader("Request for Payment - Government Fees","request_gov", "payments");
    var recServiceUpload = new FileUploader("Receipt - Service Fees","receipt_service", "receipts");
    var recGoverneUpload = new FileUploader("Receipt - Government Fees","receipt_gov", "receipts");

    reqServiceUpload.uploadFile();
    reqGoverneUpload.uploadFile();
    recServiceUpload.uploadFile();
    recGoverneUpload.uploadFile();

    reqServiceUpload.deleteFile();
    recServiceUpload.deleteFile();

    $('.ui.change.button').on('click', function() {
        $('.ui.change.form').show('slow');
    });

    $('.ui.change_submit.button').on('click', function() {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        var uri     = window.location.href;
        var form    = $('.ui.change.form');
        $.ajax({
            type: 'POST',
            url: uri,
            async: true,
            data: {
                _token      : $('#token').val(),
                total       : form.form('get value', 'total')
            },
            success: function(data) {
                $('.ui.change.form').hide('slow');
                $('#account_total').hide('slow');
                $('#account_total').text(data);
                $('#account_total').show('slow');
            }
        });
        $(this).removeClass('loading');
        $(this).removeClass('disabled');
    });

});
