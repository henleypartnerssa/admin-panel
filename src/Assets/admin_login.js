$('document').ready(function() {
    $('.ui.submit.button').on('click', function() {
        $('.ui.submit.button').addClass('loading');
        $('.ui.submit.button').addClass('disabled');
        var val = formValidation();
        if (val) {
            var formElement = $('.ui.login.form');
            $.ajax({
                type:   'POST',
                url:    '/admin',
                async:  true,
                data: {
                    _token: $('#token').val(),
                    username: formElement.form('get value', 'username'),
                    password: formElement.form('get value', 'password'),
                    otp     : Number(formElement.form('get value', 'otp'))
                },
                success: function(data) {
                    if (data === '1') {
                        window.location.href = 'admin/dashboard';
                    } else {
                        $('.ui.submit.button').removeClass('loading');
                        $('.ui.submit.button').removeClass('disabled');
                        showLogInError("Incorrect Username/Password or OTP. Please Try again.");
                    }
                }
            });
        } else {
            $('.ui.submit.button').removeClass('loading');
            $('.ui.submit.button').removeClass('disabled');
        }
    });

    $('.ui.pin.button').on('click', function() {
        $('.ui.pin.button').addClass('loading');
        $('.ui.pin.button').addClass('disabled');
        var formElement = $('.ui.login.form');
        var val = valForPin();
        if (val) {
            $.ajax({
                type:   'POST',
                url:    '/admin/send-pin',
                async:  true,
                data: {
                    _token: $('#token').val(),
                    username: formElement.form('get value', 'username'),
                    password: formElement.form('get value', 'password'),
                    otp     : Number(formElement.form('get value', 'otp'))
                },
                success: function(data) {
                    if (data === '1') {
                        $('.ui.pin.button').removeClass('loading');
                        $('.ui.pin.button').removeClass('disabled');
                        $('.ui.pin.button').text('OTP Sent');
                    } else {
                        $('.ui.pin.button').removeClass('loading');
                        $('.ui.pin.button').removeClass('disabled');
                        showLogInError("Error sending OTP. Please contact support");
                    }
                }
            });
        }
    });

    function formValidation() {
        $('.ui.login.form').form({
            fields: {
                username: {
                    indentifier: 'username',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your username'
                        },
                        {
                            type   : 'email',
                            prompt : 'Please enter valid email address'
                        }
                    ]
                },
                password: {
                    indentifier: 'password',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter passport'
                    }]
                },
                otp: {
                    indentifier: 'otp',
                    rules: [{
                        type: 'empty',
                        prompt: 'You need to provide a OTP.'
                    }]
                }
            },
            on: 'blur',
            inline: true
        });
        return $('.ui.login.form').form('is valid');
    }

    function valForPin() {
        $('.ui.login.form').form({
            fields: {
                username: {
                    indentifier: 'username',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your username'
                        },
                        {
                            type   : 'email',
                            prompt : 'Please enter valid email address'
                        }
                    ]
                },
                password: {
                    indentifier: 'password',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please enter passport'
                    }]
                }
            },
            on: 'blur',
            inline: true
        });
        return $('.ui.login.form').form('is valid');
    }

    function showLogInError(message) {
        var msgBox = $('.ui.error.message');
        msgBox.find('.header').text("Invalid Logon Details");
        msgBox.find('p').text(message);
        msgBox.show().delay(3500).fadeOut();
    }

});
