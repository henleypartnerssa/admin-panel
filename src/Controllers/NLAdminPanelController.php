<?php

namespace hpsadev\NLAdminPanel\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use hpsadev\NLAdminPanel\NLAdminPanel;
use hpsadev\NLAdminPanel\Traits\TControllerMethods;

/**
 * Admin Panel Controller Class.
 *
 * @author      Gershon Koks <gershon.koks@henleyglobal.com>
 * @copyright   Henley & Partners Ltd
 * @license     For Henley & Partners use only.
 * @version     0.0.2
 */
class NLAdminPanelController extends Controller {

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['adminLogin', 'sendPin']]);
    }

    use TControllerMethods;

}
